#include "outputdisplay.h"

#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QDebug>
#include <QTextItem>
#include <QFont>
#include "carconstants.h"

qreal OutputDisplay::getWidth() const
{
    return width;
}

void OutputDisplay::setWidth(const qreal &value)
{
    width = value;
}

qreal OutputDisplay::getHeight() const
{
    return height;
}

void OutputDisplay::setHeight(const qreal &value)
{
    height = value;
}
OutputDisplay::OutputDisplay(qreal w, qreal h, QGraphicsItem *i):width(w), height(h), QGraphicsObject(i)
{

}

void OutputDisplay::updateParams(qreal v, qreal r)
{
    vel = v;
    rot = r;

    if(scene())
    {

        width = scene()->views()[0]->width();
        height = scene()->views()[0]->height();
        scene()->update();

    }
}



QRectF OutputDisplay::boundingRect() const
{
    return shape().boundingRect();
}

QPainterPath OutputDisplay::shape() const
{
    QPainterPath p;
    p.addRect(0,0,width,height-width);
    p.addEllipse(QRectF(0,height-width,width, width));
    return QPainterPath();
}

void OutputDisplay::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)

    QRectF velRect(0, 0, width, height-width);
    qreal r = qAbs(vel)/CarConstants::maxVelocity;
    QRectF velR(0, (height-width)*0.5*((vel<0)?1:(1-r)), width, (height-width)*0.5*r);


    painter->setPen(Qt::black);
    painter->setBrush(Qt::NoBrush);
    painter->drawRect(velRect);

    QRectF rotRect(0, height-width, width, width);

    painter->drawEllipse(rotRect);

    painter->setPen(Qt::NoPen);
    painter->setBrush(Qt::red);
    painter->drawRect(velR);

    painter->drawPie(rotRect, 45*16, (int)(90*16-rot*16));


    painter->setPen(Qt::black);
    painter->setBrush(Qt::black);
    QFont font=painter->font() ;
    font.setPointSize (18);
    painter->setFont(font);

    QRectF velTextRect(0, (height - width)/2-width/2, width, width);
    painter->drawText(velTextRect, Qt::AlignCenter, QString::number(vel,'g', 2));

    QRectF rotTextRect(0, height - width/2, width, width/2);
    painter->drawText(rotTextRect, Qt::AlignCenter, QString::number(rot,'g', 2));

}

