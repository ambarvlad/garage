#ifndef SCENESTATE_H
#define SCENESTATE_H

#include "carmodel.h"
#include "garagemodel.h"
#include "obstaclemodel.h"

#include <QPointF>
#include <QPolygonF>





class SceneState
{
    CarModel * car;
    GarageModel * garage;
    QList<ObstacleModel*> obstacles;


public:
    SceneState(CarModel* c=0, GarageModel* g=0, QList<ObstacleModel*> o=QList<ObstacleModel*>()):car(c),garage(g),obstacles(o)
    {
        if(car==0)
            car = new CarModel();
        if(garage==0)
            garage = new GarageModel();
    }
    ~SceneState()
    {
        /*delete car;
        delete garage;
        qDeleteAll(obstacles);*/
    }

    SceneState* clone() const
    {
        SceneState* c = new SceneState();
        c->setCar(new CarModel(*car));
        c->setGarage(new GarageModel(*garage));
        QList<ObstacleModel*> o;
        for(int i = 0; i < obstacles.size(); ++i)
            o << new ObstacleModel(*obstacles[i]);
        c->setObstacles(o);
        return c;
    }

    void setCar(CarModel * c)
    {
        //if(car)
        //    delete car;
        car = c;
    }

    void setGarage(GarageModel * g)
    {
        //if(garage)
        //   delete garage;
        garage = g;
    }

    void setObstacles(QList<ObstacleModel*>& o)
    {
        qDeleteAll(obstacles);
        obstacles = o;
    }

    CarModel * getCar()
    {
        return car;
    }

    GarageModel * getGarage()
    {
        return garage;
    }

    QList<ObstacleModel*>& getObstacles()
    {
        return obstacles;
    }
};

#endif // SCENESTATE_H
