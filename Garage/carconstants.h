#ifndef CARCONSTANTS_H
#define CARCONSTANTS_H


class CarConstants
{
public:
    static const int maxVelocity = 5;
    static const int carWidth = 20;
    static const int carHeight = 40;
};

#endif // CARCONSTANTS_H
