#include "defuzzification.h"

defuzzification::defuzzification()
{

}
void defuzzification::add_left_sensor_tr(trapeze t)
{
    this->left_sensor.add(t);
}

void defuzzification::add_right_sensor_tr(trapeze t)
{
    this->right_sensor.add(t);
}

void defuzzification::add_center_sensor_tr(trapeze t)
{
    this->center_sensor.add(t);
}
void defuzzification::add_angle_to_garage_tr(trapeze t)
{
    this->angle_to_garage.add(t);
}

void defuzzification::add_speed_tr(trapeze t)
{
    this->speed.add(t);
}

void defuzzification::add_angle_of_wheels_tr(trapeze t)
{
    this->angle_of_wheels.add(t);
}

pair<qreal,qreal> defuzzification::get_next(SensorData sd)
{
    vector<mypair> center_sensor_val;
    vector<mypair> right_sensor_val;
    vector<mypair> left_sensor_val;
    vector<mypair> angle_val;
    if (forward)
    {
        left_sensor_val = left_sensor.crossed_trapeze(sd.front1);
        right_sensor_val = right_sensor.crossed_trapeze(sd.front3);
        center_sensor_val = center_sensor.crossed_trapeze(sd.front2);
    }
    else
    {
        left_sensor_val = left_sensor.crossed_trapeze(sd.back1);
        right_sensor_val = right_sensor.crossed_trapeze(sd.back3);
        center_sensor_val = center_sensor.crossed_trapeze(sd.back2);
    }
    angle_val = left_sensor.crossed_trapeze(sd.angle);

    vector< pair< vector<int>, qreal> > rules;

    for (int i1= 0; i1 < left_sensor_val.size(); ++i1)
        for(int i2 = 0; i2 < center_sensor_val.size(); ++i2)
            for (int i3 = 0; i3 < right_sensor_val.size();++i3)
                for (int i4 = 0; i4 < angle_val.size();++i4)
                {
                    rules.push_back(pair< vector<int>, qreal >());
                    rules[rules.size() - 1].first.push_back(left_sensor_val[i1].number);
                    rules[rules.size() - 1].first.push_back(center_sensor_val[i2].number);
                    rules[rules.size() - 1].first.push_back(right_sensor_val[i3].number);
                    rules[rules.size() - 1].first.push_back(angle_val[i4].number);
                    rules[rules.size() - 1].second =
                            minimum(left_sensor_val[i1].conf_level,
                            center_sensor_val[i2].conf_level,
                            right_sensor_val[i3].conf_level,
                            angle_val[i4].conf_level);
                }
    for (int i = 0; i < rules.size(); ++i)
    {
        //TODO Ищем правило и устанавливаем планки для выходных параметров.
    }

    //TODO Обрезаем выходные параметры по планкам.

    //TODO Ищем центры масс полученных фигур.
}

qreal minimum(qreal r1,qreal r2, qreal r3, qreal r4)
{
    if (r1 <= r2 && r1 <= r2 && r1 <= r3) return r1;
    if (r2 <= r1 && r2 <= r3 && r2 <= r4) return r2;
    if (r3 <= r1 && r3 <= r2 && r3 <= r4) return r3;
    return r4;
}
