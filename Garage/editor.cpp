#include "editor.h"

#include <QMouseEvent>
#include <QDebug>
#include <QMenu>
#include <QScrollBar>


Editor::Editor(QWidget *parent)
    : QGraphicsView(parent), selectedItem(0)
{
    scene = new QGraphicsScene(this);
    scene->setSceneRect(-400,-300, 800, 600);

    sceneState = new SceneState();
    selection = new Selection();

    this->setScene(scene);

}

Editor::~Editor()
{
    /*scene->clear();
    delete selection;
    delete car;
    delete garage;
    delete scene;*/
}

void Editor::resetScene()
{
    scene->clear();
    scene->addItem(sceneState->getCar());
    scene->addItem(sceneState->getGarage());
}

void Editor::setSlider(QSlider *slider)
{
    zoomSlider = slider;
    connect(slider, SIGNAL(valueChanged(int)), this, SLOT(changeScale(int)));
}

SceneState *Editor::getSceneState()
{
    return sceneState;
}

void Editor::selectItem(QGraphicsItem* item)
{

    if(item==selection || (selection && item==selection->selectedItem()))
        return;
    if(selection->scene()!=scene)
        scene->addItem(selection);
    selection->setSelectedItem(item);

}
void Editor::deselectItem()
{
    if(selection->scene() == scene)
    {
        selection->clearSelection();
        scene->removeItem(selection);
    }
}

void Editor::mouseReleaseEvent(QMouseEvent *event)
{

    QGraphicsView::mouseReleaseEvent(event);
    dragging = false;
}

void Editor::mousePressEvent(QMouseEvent *event)
{
    QGraphicsView::mousePressEvent(event);
    if(event->isAccepted())
        return;

    QGraphicsItem* i = this->itemAt(event->x(), event->y());
    if(i)
        selectItem(i);
    else
    {
        deselectItem();
        dragPoint=event->pos();
        dragging = true;
    }


}

void Editor::mouseMoveEvent(QMouseEvent *event)
{
    QGraphicsView::mouseMoveEvent(event);
    if(dragging)
    {

        QPointF d(event->x()-dragPoint.x(), event->y()-dragPoint.y());
        this->verticalScrollBar()->setValue(verticalScrollBar()->value()-d.y());
        this->horizontalScrollBar()->setValue(horizontalScrollBar()->value()-d.x());

        dragPoint = event->pos();

    }
}

void Editor::wheelEvent(QWheelEvent *event)
{

    qreal d = 1.0+event->angleDelta().y()/1200.0;
    this->scale(d,d);
    if(zoomSlider)
        zoomSlider->setValue(this->transform().m11()*100);

}

void Editor::contextMenuEvent(QContextMenuEvent *event)
{
    QGraphicsItem* i = this->itemAt(event->x(), event->y());
    CarModel * car = sceneState->getCar();
    GarageModel * garage = sceneState->getGarage();

    if(i==car||i==garage||(i==selection && (selection->selectedItem()==car||selection->selectedItem()==garage)))
        return;

    QMenu* menu = new QMenu(this);

    if(i)
        menu->addAction("&Delete obstacle", this, SLOT(deleteObstacle()));
    else
        menu->addAction("&Add obstacle", this, SLOT(addObstacle()));
    mousePos=event->pos();
    menu->popup(mapToGlobal(event->pos()));

}

void Editor::deleteObstacle()
{
    if(!selection)
        return;
    ObstacleModel* m = dynamic_cast<ObstacleModel*>(selection->selectedItem());
    sceneState->getObstacles().removeAll(m);
    scene->removeItem(m);
    deselectItem();
}

void Editor::addObstacle()
{
    ObstacleModel* m = new ObstacleModel();
    sceneState->getObstacles() << m;

    m->setPos(this->mapToScene(mousePos.toPoint()));
    scene->addItem(m);
}

void Editor::changeScale(int val)
{
    this->setTransform(this->transform().fromScale(val/100.0, val/100.0));
}
