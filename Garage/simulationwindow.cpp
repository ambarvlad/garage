#include "simulationwindow.h"
#include "ui_simulationwindow.h"

SimulationWindow::SimulationWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SimulationWindow)
{
    ui->setupUi(this);
    ui->graphicsView->setFocus();
    ui->outputView->setScene(new QGraphicsScene());
    ui->graphicsView->setOutDisplay(new OutputDisplay());
    ui->outputView->scene()->addItem(ui->graphicsView->getOutDisplay());

}

SimulationWindow::~SimulationWindow()
{
    delete ui;
}

void SimulationWindow::loadScene(const SceneState *sceneState)
{
    ui->graphicsView->loadScene(sceneState);
}
