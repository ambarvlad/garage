#ifndef SIMULATIONWINDOW_H
#define SIMULATIONWINDOW_H

#include "scenestate.h"

#include <QMainWindow>

namespace Ui {
class SimulationWindow;
}

class SimulationWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit SimulationWindow(QWidget *parent = 0);
    ~SimulationWindow();
    void loadScene(const SceneState* sceneState);
private:
    Ui::SimulationWindow *ui;
};

#endif // SIMULATIONWINDOW_H
