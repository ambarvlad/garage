#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->graphicsView->resetScene();
    ui->graphicsView->setSlider(ui->verticalSlider);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionRunSimulation_triggered()
{
    simulation = new SimulationWindow(this);
    simulation->loadScene(ui->graphicsView->getSceneState());
    simulation->show();
}
