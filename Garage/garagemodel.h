#ifndef GARAGEMODEL_H
#define GARAGEMODEL_H

#include <QGraphicsItem>



class GarageModel : public QGraphicsItem
{
public:
    GarageModel();
    GarageModel(const GarageModel& g);
    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    enum { Type = UserType + 2 };

    int type() const
    {
       return Type;
    }
protected:

    QColor color;
    QPolygonF polygon_shape;
    void initPolygon();
};

#endif // GARAGEMODEL_H
