#ifndef SIMULATION_H
#define SIMULATION_H


#include <QGraphicsView>
#include <QContextMenuEvent>
#include <QAction>
#include <QSlider>
#include <QTimer>
#include "carcontroller.h"
#include "carmodel.h"
#include "garagemodel.h"
#include "obstaclemodel.h"
#include "outputdisplay.h"
#include "scenestate.h"
#include "selection.h"


class Simulation : public QGraphicsView
{
    Q_OBJECT
    QGraphicsScene* scene;
    SceneState* sceneState;
    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
    void keyReleaseEvent(QKeyEvent *event) Q_DECL_OVERRIDE;

    QPointF mousePos;

    QTimer* animationTimer;

    CarController* controller;
    OutputDisplay* outDisplay;

protected slots:
    void simulationUpdate();
signals:
    void keyPressSignal(QKeyEvent*);
    void keyReleaseSignal(QKeyEvent*);

public:
    Simulation(QWidget *parent = 0);
    ~Simulation();
    void loadScene(const SceneState* sceneState);
    OutputDisplay *getOutDisplay() const;
    void setOutDisplay(OutputDisplay *value);
};

#endif // SIMULATION_H
