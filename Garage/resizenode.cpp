#include "resizenode.h"

#include <QDebug>
#include <QGraphicsScene>

ResizeNode::ResizeNode(QGraphicsItem *parent, QPointF * n):QGraphicsItem(parent), node(n)
{
    setPos(QPointF(n->x(),n->y()));
}

ResizeNode::~ResizeNode()
{
}

QRectF ResizeNode::boundingRect() const
{
    return QRectF(-5,-5,10,10);
}

QPainterPath ResizeNode::shape() const
{
    QPainterPath p;
    p.addEllipse(QPointF(0,0), 4, 4);
    return p;
}

void ResizeNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    painter->setBrush(QBrush(Qt::red));
    painter->drawPath(shape());
    Q_UNUSED(option);
    Q_UNUSED(widget);
}


void ResizeNode::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    dragging = false;
    Q_UNUSED(event);
    event->accept();
}

void ResizeNode::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    dragging = true;
    event->accept();
}

void ResizeNode::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if(dragging)
    {
        setPos(this->mapToParent(mapFromScene(event->scenePos())));

        node->setX(pos().x());
        node->setY(pos().y());
        scene()->update();
    }
    event->accept();
}

