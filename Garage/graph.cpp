#include "graph.h"

graph::graph()
{

}

// Добавление трапеции к графику.
void graph::add(trapeze t)
{
    tr.push_back(t);
}

// Номера и степени уверенности пересеченных трапеций.
vector<mypair> graph::crossed_trapeze(qreal x0)
{
    vector<mypair> v;
    for (int i = 0; i < tr.size(); ++i)
        if (tr[i].x_under(x0))
            v.push_back(mypair(i,tr[i].get_y(x0)));
    return v;
}

// Образка трапеций.
graph graph::cut(vector<qreal> y)
{
    graph g;
    for (int i = 0; i < tr.size(); ++i)
        g.tr.push_back(tr[i].cut(y[i]));
    return g;
}

qreal graph::center_of_mass()
{
    qreal S,V,k,b;
    S = 0;
    V = 0;
    QPointF p1,p2;
    V = 0;
    QPolygonF united_polygon;
    for(int i = 0; i < tr.size(); ++i)
        united_polygon = united_polygon.united(tr[i].vmp);
    for (int i = 0; i < united_polygon.size()-2; ++i)
    {
        p1 = united_polygon[i];
        p2 = united_polygon[i+1];
        if ((p2.x() - p1.x()))
        {
            k = (p2.y() - p1.y())/(p2.x() - p1.x());
            b = p1.y() - k*p1.x();
            S += integral_f(k,b,p1.x(),p2.x());
            V += integral_f2(k,b,p1.x(),p2.x());
        }
    }
    return V/(2*S);
}

qreal integral_f(qreal k,qreal b,qreal x1,qreal x2)
{
    return (k*x2*x2/2 + b*x2) - (k*x1*x1/2 + b*x1);
}

qreal integral_f2(qreal k,qreal b,qreal x1,qreal x2)
{
    return (k*k*x2*x2*x2/3 + k*b*x2*x2 + b*b*x2) - (k*k*x1*x1*x1/3 + k*b*x1*x1 + b*b*x1);
}

