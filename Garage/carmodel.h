#ifndef CARMODEL_H
#define CARMODEL_H

#include <QGraphicsItem>


class CarModel : public QGraphicsItem
{

public:
    CarModel();
    CarModel(const CarModel& c);
    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    QPointF getRightFrontWheel();

    QPointF getLeftFrontWheel();

    QPointF getRightBackWheel();

    QPointF getLeftBackWheel();

    enum { Type = UserType + 1 };

    int type() const
    {
       return Type;
    }
protected:
    void advance();

    QColor color;
    QPolygonF polygon_shape;

    void initPolygon();
};

#endif // CARMODEL_H
