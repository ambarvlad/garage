#ifndef OBSTACLEMODEL_H
#define OBSTACLEMODEL_H

#include <QGraphicsItem>
#include <QPainter>



class ObstacleModel : public QGraphicsItem
{
public:
    ObstacleModel();
    ObstacleModel(const QPolygonF& polygon);
    ObstacleModel(const ObstacleModel& o);

    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QPointF* getData();
protected:

    QColor color;
    QPolygonF polygon_shape;
};

#endif // OBSTACLEMODEL_H
