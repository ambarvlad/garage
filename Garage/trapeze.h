#ifndef TRAPEZE_H
#define TRAPEZE_H

#include "mypoint.h"
#include <vector>
#include <QPolygonF>
using namespace std;

class trapeze
{
public:
    QPolygonF vmp;

    trapeze(qreal,qreal,qreal,qreal);
    trapeze(qreal,qreal,qreal,qreal,qreal,qreal);

    bool x_under(qreal);
    double get_y(qreal);
    trapeze cut(qreal);
};

#endif // TRAPEZE_H
