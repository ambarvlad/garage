#include "obstaclemodel.h"

#include <QPainter>

ObstacleModel::ObstacleModel(const QPolygonF &polygon)
{
    polygon_shape = polygon;

}

ObstacleModel::ObstacleModel(const ObstacleModel &o):polygon_shape(o.shape().toFillPolygon())
{
    setPos(o.pos());
    setRotation(o.rotation());

}

ObstacleModel::ObstacleModel()
{
    polygon_shape << QPointF(-10,-10)
                  << QPointF(10, -10)
                  << QPointF(10, 10)
                  << QPointF(-10, 10);

}

QRectF ObstacleModel::boundingRect() const
{
    return polygon_shape.boundingRect();
}

QPainterPath ObstacleModel::shape() const
{
    QPainterPath p;
    p.addPolygon(polygon_shape);

    return p;
}

void ObstacleModel::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(color);
    painter->drawConvexPolygon(polygon_shape);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

QPointF *ObstacleModel::getData()
{
    return polygon_shape.data();
}

