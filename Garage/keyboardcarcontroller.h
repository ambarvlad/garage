#ifndef KEYBOARDCARCONTROLLER_H
#define KEYBOARDCARCONTROLLER_H

#include "carcontroller.h"

#include <QKeyEvent>
#include <QDebug>


class KeyboardCarController : public CarController
{
    Q_OBJECT
    bool forward, backward, right, left;

public:
    KeyboardCarController(CarModel* c):CarController(c){reset();}

    void updateController(SensorData sd);

    virtual void reset()
    {
        CarController::reset();
        forward = backward = right = left = false;
    }

public slots:
    void keyPressEvent(QKeyEvent * event);
    void keyReleaseEvent(QKeyEvent * event);
};

#endif // KEYBOARDCARCONTROLLER_H
