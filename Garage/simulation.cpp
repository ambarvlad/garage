#include "keyboardcarcontroller.h"
#include "simulation.h"



#include <QMouseEvent>
#include <QDebug>
#include <QMenu>
#include <QScrollBar>


Simulation::Simulation(QWidget *parent)
    : QGraphicsView(parent), outDisplay(0)
{
    scene = new QGraphicsScene(this);
    scene->setSceneRect(-400,-300, 800, 600);

    this->setScene(scene);
    this->animationTimer = new QTimer(this);
    connect(animationTimer, SIGNAL(timeout()), this, SLOT(simulationUpdate()));

}

void Simulation::loadScene(const SceneState *sceneState)
{
    this->sceneState = sceneState->clone();
    scene->addItem(this->sceneState->getCar());
    scene->addItem(this->sceneState->getGarage());
    for(QList<ObstacleModel*>::iterator i = this->sceneState->getObstacles().begin(); i!=this->sceneState->getObstacles().end();++i)
        scene->addItem(*i);
    animationTimer->start((int)(1000.0/60));

    KeyboardCarController* c = new KeyboardCarController(this->sceneState->getCar());

    connect(this,  SIGNAL(keyPressSignal(QKeyEvent*)), c, SLOT(keyPressEvent(QKeyEvent*)));
    connect(this,  SIGNAL(keyReleaseSignal(QKeyEvent*)), c, SLOT(keyReleaseEvent(QKeyEvent*)));

    controller = c;
}

Simulation::~Simulation()
{
    scene->clear();
    if(animationTimer)
        animationTimer->stop();
    delete controller;
    delete animationTimer;
    delete scene;
}

void Simulation::keyPressEvent(QKeyEvent *event)
{

    emit(keyPressSignal(event));
    //QGraphicsView::keyPressEvent(event);
}

void Simulation::keyReleaseEvent(QKeyEvent *event)
{
    emit(keyReleaseSignal(event));
    //QGraphicsView::keyReleaseEvent(event);
}
OutputDisplay *Simulation::getOutDisplay() const
{
    return outDisplay;
}

void Simulation::setOutDisplay(OutputDisplay *value)
{
    outDisplay = value;
    outDisplay->updateParams(0,0);
}


void Simulation::simulationUpdate()
{
    controller->update(SensorData());
    this->centerOn(sceneState->getCar());
    if(outDisplay)
        outDisplay->updateParams(controller->getVelocity(), controller->getTheta());


}
