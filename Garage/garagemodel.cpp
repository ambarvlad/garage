#include "garagemodel.h"

#include <QDebug>
#include <QPainter>

GarageModel::GarageModel()
{
    initPolygon();

}

GarageModel::GarageModel(const GarageModel &g)
{
    setPos(g.pos());
    setRotation(g.rotation());
    initPolygon();
}

void GarageModel::initPolygon()
{
    polygon_shape << QPointF(-30, -30) <<
                     QPointF(30, -30) <<
                     QPointF(30, 30) <<
                     QPointF(20, 30) <<
                     QPointF(20, -20) <<
                     QPointF(-20, -20) <<
                     QPointF(-20, 30) <<
                     QPointF(-30, 30);
    color.setRgb(255, 0, 0);
}

QRectF GarageModel::boundingRect() const
{
    return QRectF(-30, -30, 60, 60);
}

QPainterPath GarageModel::shape() const
{
    QPainterPath p;
    p.addPolygon(polygon_shape);

    return p;
}

void GarageModel::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(color);
    painter->drawConvexPolygon(polygon_shape);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}


