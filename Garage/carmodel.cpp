#include "carmodel.h"

#include <QPainter>
#include "carconstants.h"

CarModel::CarModel()
{
    initPolygon();
}

CarModel::CarModel(const CarModel &c)
{
    setPos(c.pos());
    setRotation(c.rotation());
    initPolygon();
}

void CarModel::initPolygon()
{
    polygon_shape << QPointF(0, -CarConstants::carHeight/2) <<
                     QPointF(CarConstants::carWidth/2, -CarConstants::carHeight*0.3) <<
                     QPointF(CarConstants::carWidth/2, CarConstants::carHeight/2) <<
                     QPointF(-CarConstants::carWidth/2, CarConstants::carHeight/2) <<
                     QPointF(-CarConstants::carWidth/2, -CarConstants::carHeight*0.3);
    color.setRgb(255, 0, 0);
}

QRectF CarModel::boundingRect() const
{
    return shape().boundingRect();
}

QPainterPath CarModel::shape() const
{
    QPainterPath p;
    p.addPolygon(polygon_shape);
    return p;
}

void CarModel::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(color);
    painter->drawConvexPolygon(polygon_shape);

    Q_UNUSED(option);
    Q_UNUSED(widget);
}

QPointF CarModel::getRightFrontWheel()
{
    return QPointF(CarConstants::carWidth/2, -CarConstants::carHeight*0.3);
}

QPointF CarModel::getLeftFrontWheel()
{
    return QPointF(-CarConstants::carWidth/2, -CarConstants::carHeight*0.3);
}

QPointF CarModel::getRightBackWheel()
{
    return QPointF(CarConstants::carWidth/2, CarConstants::carHeight*0.3);
}

QPointF CarModel::getLeftBackWheel()
{
    return QPointF(-CarConstants::carWidth/2, CarConstants::carHeight*0.3);
}


