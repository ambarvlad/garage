#-------------------------------------------------
#
# Project created by QtCreator 2015-04-29T20:46:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Garage
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    editor.cpp \
    carmodel.cpp \
    garagemodel.cpp \
    selection.cpp \
    obstaclemodel.cpp \
    resizenode.cpp \
    scenestate.cpp \
    simulationwindow.cpp \
    simulation.cpp \
    carcontroller.cpp \
    sensordata.cpp \
    keyboardcarcontroller.cpp \
    outputdisplay.cpp \
    carconstants.cpp \
    trapeze.cpp \
    mypoint.cpp \
    graph.cpp \
    defuzzification.cpp \
    mypair.cpp

HEADERS  += mainwindow.h \
    editor.h \
    carmodel.h \
    garagemodel.h \
    selection.h \
    obstaclemodel.h \
    resizenode.h \
    scenestate.h \
    simulationwindow.h \
    simulation.h \
    carcontroller.h \
    sensordata.h \
    keyboardcarcontroller.h \
    outputdisplay.h \
    carconstants.h \
    trapeze.h \
    mypoint.h \
    graph.h \
    defuzzification.h \
    mypair.h

FORMS    += mainwindow.ui \
    simulationwindow.ui

DISTFILES +=
